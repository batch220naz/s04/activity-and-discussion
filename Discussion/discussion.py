class Car() :
	def __init__(self, brand, model, year_of_model) :
		self.brand = brand
		self.model = model
		self.year_of_model = year_of_model

		self.fuel = "Gasoline"
		self.fuel_level = 0

	def __repr__(self):
		return f"the car is a {self.brand} {self.model}"

	# Methods
	def fill_fuel(self):
		print(f"Currnt fuel level: {self.fuel_level}")
		print("Filling up the fuel tank...")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")

new_car = Car("Honda", "Jazz", 2020)

print(f"My new car is a {new_car.brand} {new_car.model} {new_car.year_of_model}")

print(new_car)




# [SECTION] Encapsulation
# a form of data hiding in python

class Person():
	"""docstring for Person"""
	def __init__(self):
		self._name = "John Doe"
		
	def set_name(self, name):
		self._name = name

	def get_name(self):
		print(f"Name of Person: {self._name}")

p1 = Person()
p1.get_name()
p1.set_name("Jane Doe")
p1.get_name()
print(p1._name)
print("")


"""
Mini Activity
- add another protected attribute called age and create the necessary getter and setter method
"""
# Code 1
class Person():
	"""docstring for Person"""
	def __init__(self):
		self._age = "18"
		
	def set_age(self, age):
		self._age = age

	def get_age(self):
		print(f"Age of Person: {self._age}")

p1 = Person()
p1.get_age()
p1.set_age("19")
p1.get_age()
print("")

# Code 2
class Person():
	"""docstring for Person"""
	def __init__(self):
		self._name = "John Doe"
		self._age = 18
		
	def set_name_and_age(self, name, age):
		self._name = name
		self._age = age

	def get_name_and_age(self):
		print(f"Name of Person: {self._name}")
		print(f"Age of Person: {self._age}")

p1 = Person()
p1.get_name_and_age()
p1.set_name_and_age("Jane Doe", 21)
p1.get_name_and_age()
print(p1._name)
print(p1._age)
print("")


# [SECTION] Inheritance
class Employee(Person):
	"""docstring for Employee"""
	def __init__(self, employee_id):
		# super() can be used to invoke immediate parent class constructor
		super().__init__()
		self._employee_id = employee_id

	def get_employee_id(self):
		print(f"The employee Id is  {self._employee_id}")
	
	def set_employee_id(self, employee_id):
		self._employee_id = employee_id

	def get_details(self):
		print(f"{self._employee_id} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
print("\n")



"""
Mini Activity
"""


class Student(Person):
	"""docstring for Student"""
	def __init__(self, student_number, course, year_level):
		super().__init__()
		self._student_number = student_number
		self._course = course
		self._year_level = year_level

	def get_student_details(self):
		print(f"The number of the Student is {self._student_number}")
		print(f"The course of the Student is {self._course}")
		print(f"The year model of the Student is {self._year_level}")
	
	def set_student_details(self, student_number, course, year_level):
		self._student_number = student_number
		self._course = course
		self._year_level = year_level

	def get_all_details(self):
		print(f"{self._name} is currently in {self._year_level} year taking up {self._course}")

stud1 = Student("STUD-010", "IT", "4th")
stud1.get_all_details()
print("\n")





# [SECTION] Polymorphism
# Since not all methods are applicable to the child element where inherited, some need to be re-defined/re-implemented

# Using Function
class Admin():
	"""docstring for Admin"""
	def isAdmin(self) :
		print(True)
	def user_type(self) :
		print("Admin User")
		print("")

class Customer():
	"""docstring for Customer"""
	def isAdmin(self) :
		print(False)
	def user_type(self) :
		print("Regular User")

def test_function(obj):
	obj.isAdmin()
	obj.user_type()

user_admin = Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)
print("\n")



# Using Classes
class Team_Lead():
	def occupation(self):
		print("Team_Lead")

	def hasAuth(self):
		print(True)

class Team_Member():
	def occupation(self):
		print("Team_Member")

	def hasAuth(self):
		print(False)

tl1 = Team_Lead()
tm1 = Team_Member()

for person in (tl1, tm1):
	person.occupation()

# Using Inheritance
class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks (developer career, pi-shaped, and short courses)")
	def num_of_hours(self):
		print("Learn web dev in 360 hours")

class Developer_Career(Zuitt):
	def num_of_hours(self):
		print("Learn the basic of web development in 240 hours")

class Pi_Shaped():
	def num_of_hours(self):
		print("Learn the basic of web development in 140 hours")

class Short_Courses():
	def num_of_hours(self):
		print("Learn the basic of web development in 20 hours")

course1 = Developer_Career()
course2 = Pi_Shaped()
course3 = Short_Courses()

for course in (course1, course2, course3):
	course.num_of_hours()
	# course.tracks() -- Error cannot be accessed if there is no "Zuitt" inherited


# [SECTION] Abstraction

from abc import ABC, abstractclassmethod

class Polygon(ABC):
	@abstractclassmethod
	def activity(self):
		# pass denote that the method does not do anything
		pass

	def not_overriden(self):
		pass

class Triangle(Polygon) :
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print(f"This is has 3 sides")

class Pentagon(Polygon) :
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print(f"This is has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()
shape1.print_number_of_sides()

		