class Dog():
	"""docstring for Dog"""
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def get_dog_details(self):
		print(f"The name of the Pet is {self._name}")
		print(f"The breed of the Pet is {self._breed}")
		print(f"The age of the Pet is {self._age}")
	
	def set_dog_details(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def call(self):
		print(f"Here {self._name}!")



class Cat():
	"""docstring for Dog"""
	def __init__(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def get_cat_details(self):
		print(f"The name of the Pet is {self._name}")
		print(f"The breed of the Pet is {self._breed}")
		print(f"The age of the Pet is {self._age}")
	
	def set_cat_details(self, name, breed, age):
		self._name = name
		self._breed = breed
		self._age = age

	def call(self):
		print(f"{self._name}, Come on!")


dog1 = Dog("Isis", "Dalmatian", 15)
dog1.call()
print("\n")
cat1 = Cat("Puss", "Persian", 15)
cat1.call()
print("\n")
